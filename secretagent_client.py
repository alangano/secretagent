
# Module for use in applications

import os
import socket
import json

g_datadir = f"{os.environ['HOME']}/.secretagent"
g_socket_file = f"{g_datadir}/secretagent.sock"

def find(*i_args):
   if len(i_args) == 0:
      raise Exception('invalid number of params')

   return json.loads(_call({'function':'find','args':list(i_args)}))

def _call(i_data):
   t_socket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
   t_socket.connect(g_socket_file)
   t_socket.settimeout(30)
   t_socket.send(json.dumps(i_data).encode())
   t_raw_response = t_socket.recv(4096)
   return t_raw_response.decode()

