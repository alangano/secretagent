
# Introduction

secretagent is a local secrets vault, inspired by ssh-agent.

The big point is that plaintext secrets (passwords, api-keys, et-al) are not stored on disk.

Neither are secrets half-assed encrypted, with the password ... in cleartext, on disk.

You edit your secrets file, which is stored encrypted.

secretagent runs a daemon in a Linux user account.  You load your secrets file into the running daemon.

In both of these cases, the user manually enters the password.

Applications query the daemon to extract secrets for use.

# Installation

secretagent was written under Python 3.6.15.  3.6 is currently in wide use on enterprise Linux systems.

## Needed Modules
```
pip3 install pynacl --user
pip3 install daemonize --user
```

The file 'secretagent' can be put anywhere.

Data is stored in $HOME/.secretagent


# Examples

## First Edit
```
secretagent edit
```

During this first execution of secrets-edit, you will be asked for the password.  The password you give is used for all other functions.

The first edit will contain example entries, which you can keep to do away with as you wish.

env-var EDITOR is used to determine the editor to run, with vi as a default.



## datafile Format
It will become apparent, when edited, that the format of the datafile is JSON.

You are allowed full-line comments.  Leading spaces, from the beginning of the line, are allowed.

### Basic Schema
A secrets entry looks like this:
```
   {
      "key": [ "key-part-1", "key-part-2", "key-part-3" ],
      "secret": "wut wut!"
   },
```
The "key" consists of multiple parts.

There can be a varying number of key parts between entries, depending on how you design your naming.

When querying, the app supplies the key values, and secretagent matches against the key-parts in the datafile.


### Defaulted Datafile Entries

One (or more?) of these parts can have a special values of "\*" (asterisk)

The asterisk is a wildcard.  This allows you to have a default entry, with more specific entries as overrides.

e.g., database account SYS has a standard password across all databases, except for a few rogue accounts.
```
   { "key": [ "db", "*", "sys" ], "secret": "default-password" },
   { "key": [ "db", "database1", "sys" ], "secret": "db1-password" },
   { "key": [ "db", "database2", "sys" ], "secret": "db1-password" },
   { "key": [ "db", "database3", "sys" ], "secret": "db1-password" },
```


## Start The Daemon
```
secretagent start --daemon
```

secretagent will start and daemonize.

## Load the Secrets File

```
secretagent load
```

This will ask for the password.  The password is passed to the daemon, which reads and decrypts the datafile.

After loading the datafile, the daemon can be queried.

## Changing the Secrets

**Edit the File**
```
secretagent edit
```

**Reload the Daemon**
```
secretagent load
```

## Querying the Daemon


### CLI
```
secretagent find <keypart> [ <keypart> [ ... ] ]
```

The CLI form is suitable for use in shell scripts.  Pass the output to jq and extract the payload.

```
$ ./secretagent find db mydb1 sys | jq '.payload' --raw-output
default-password
```

### As a library in Python

example.py:
```
#!/usr/bin/env python3

# Example Usage in Application

from secretagent_client import find as sagent_find

print(sagent_find('db','duper_db','sys'))
print(sagent_find('db','database3','sys'))
```

## Debugging
### Run the server in foreground with debug
```
secretagent start --debug
```

This puts debug on STDERR.

**WARNING:** This _will_ include passwords!

